package Tasks_Package;

import Tasks_Package.GeomFigure;
import Tasks_Package.WithAngels;

import java.util.Scanner;

public class Polygon extends GeomFigure implements WithAngels {
    private int numberOfVertex;
    public double[] coordinates;

    public Polygon() {
        System.out.print("Input number of vertex: ");
        Scanner input = new Scanner(System.in);
        this.numberOfVertex = input.nextInt();
        coordinates = new double[2 * numberOfVertex];
    }

    public Polygon(int init_number_vertex) {
        this.numberOfVertex = init_number_vertex;
        this.coordinates = new double[2 * numberOfVertex];
    }

    public double getArea() {
        System.out.println("Can't out Area");
        return 0;
    }

    public double getPerimeter() {
        System.out.println("Can't out Perimeter");
        return 0;
    }

    public void setCoordinates(){
        System.out.println("Input first x, then y");
        for (int i = 0; i < 2 * numberOfVertex; ++i){
            Scanner coordinate = new Scanner(System.in);
            coordinates[i] = coordinate.nextDouble();
        }
    }
    
    public void printVertex() {
        for (int i = 0; i < 2 * numberOfVertex; ++i) {
            if (i % 2 == 0) {
                System.out.print("vertex: x = " + coordinates[i]);
            }
            else{
                System.out.println(" vertex: y = " + coordinates[i]);
            }
        }
    }

}
