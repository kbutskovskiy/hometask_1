package Tasks_Package;

public abstract class GeomFigure {
    public abstract double getArea();

    public abstract double getPerimeter();
}
