package Tasks_Package.Figures;

import Tasks_Package.GeomFigure;

public class Circle extends GeomFigure {
    private double radius;
    private double xCenter;
    private double yCenter;
    private String color;

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    public void colorCircle(String newColor) {
        this.color = newColor;
    }

    public void setRadius(double newRadius) {
        this.radius = newRadius;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getCenter_x() {
        return this.xCenter;
    }

    public double getCenter_y() {
        return this.yCenter;
    }

    public String getColor() {
        return this.color;
    }

    public void getCenter_x(double new_center_x){
        this.xCenter = new_center_x;
    }

    public void getCenter_y(double new_center_y){
        this.yCenter = new_center_y;
    }
}
