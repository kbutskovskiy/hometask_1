package Tasks_Package.Figures;

import Tasks_Package.GeomFigure;

public class Triangle extends GeomFigure {
    private double firstSide;
    private double secondSide;
    private double base;
    private double height;

    @Override
    public double getPerimeter() {
        return firstSide + secondSide + base;
    }

    @Override
    public double getArea() {
        return 0.5 * base * height;
    }

    public void setHeight(double newHeight) {
        this.height = newHeight;
    }

    public void setBase(double newBase) {
        this.base = newBase;
    }

    public void setFirstSide(double newFirstSide) {
        this.firstSide = newFirstSide;
    }

    public void setSecondSide(double newSecondSide) {
        this.secondSide = newSecondSide;
    }

    public double getFirstSide(){
        return this.firstSide;
    }

    public double getSecondSide(){
        return this.secondSide;
    }

    public double getBase(){
        return this.base;
    }

    public double getHeight(){
        return this.height;
    }
}
