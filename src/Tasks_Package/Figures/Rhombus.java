package Tasks_Package.Figures;

import Tasks_Package.Polygon;
import Tasks_Package.SidesEqual;

public class Rhombus extends Polygon implements SidesEqual {
    private double length;
    private double diagonalFirst;
    private double diagonalSecond;


    @Override
    public void printSideLength() {
        System.out.println(length);
    }

    @Override
    public double getArea() {
        setDiagonals(coordinates[0], coordinates[1], coordinates[4], coordinates[5],
                coordinates[2], coordinates[3], coordinates[6], coordinates[7]);
        return (0.5 * diagonalFirst * diagonalSecond);
    }

    @Override
    public double getPerimeter() {
        return length * 4.;
    }

    public void setLength(double xFirstVertex, double yFirstVertex,
                          double xSecondVertex, double ySecondVertex) {
        this.length = Math.sqrt(Math.pow((xFirstVertex - xSecondVertex), 2.) +
                Math.pow((ySecondVertex - yFirstVertex), 2.));
        ;
    }

    public void setDiagonals(double xFirstDiagonal1, double yFirstDiagonal1,
                             double xSecondDiagonal1, double ySecondDiagonal1, double xFirstDiagonal2,
                             double yFirstDiagonal2, double xSecondDiagonal2, double ySecondDiagonal2) {
        // Находим длину вектора, выглядит сложно, но это простая формула.
        diagonalFirst = Math.sqrt(Math.pow((xFirstDiagonal1 - xSecondDiagonal1), 2.) +
                Math.pow((ySecondDiagonal1 - yFirstDiagonal1), 2.));
        diagonalSecond = Math.sqrt(Math.pow((xFirstDiagonal2 - xSecondDiagonal2), 2.)
                + Math.pow((ySecondDiagonal2 - yFirstDiagonal2), 2.));
    }

    public double getDiagonal1(){
        return diagonalFirst;
    }

    public double getDiagonal2(){
        return diagonalSecond;
    }
}
