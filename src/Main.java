import Tasks_Package.Figures.Rhombus;

public class Main {
    public static void main(String[] args){
        Rhombus rhombus = new Rhombus();
        rhombus.setCoordinates();
        rhombus.setLength(rhombus.coordinates[0], rhombus.coordinates[1], rhombus.coordinates[2],
                rhombus.coordinates[3]);
        rhombus.printSideLength();
        System.out.println(rhombus.getPerimeter());
        System.out.println(rhombus.getArea());
        rhombus.printVertex();
    }
}
